import subprocess
import os
import sys

def isccppfile(filename):
    return filename.endswith('.h') \
        or filename.endswith('.c') \
        or filename.endswith('.hpp') \
        or filename.endswith('.cpp') \

if __name__ == "__main__":
    args = sys.argv[1:]
    if len(args) < 1:
        print('Usage: python3 run-clang-format.py <dir>')
        exit(1)

    dir   = args[0]
    
    style = ('BasedOnStyle: Google, '
    	     'IndentWidth: 2, '
    	     'AlignAfterOpenBracket: Align, '
    	     'AlignConsecutiveAssignments: true, '
    	     'AlignConsecutiveDeclarations: true, '
    	     'AlignTrailingComments: true, '
    	     'AllowShortIfStatementsOnASingleLine: false, '
    	     'BreakBeforeBraces: Allman, '
    	     'PointerAlignment: Right, '
    	     'SpaceAfterCStyleCast: true, '
    	     'SpacesBeforeTrailingComments: 1, '
    	     'TabWidth: 4, '
    	     'UseTab: Never')

    for dir, _, files in os.walk(dir):
        for filename in files:
            filepath = dir + '/' + filename
            if isccppfile(filename):
                cmd = 'clang-format -i -style=' + '"{' + style + '}"' + ' ' + filepath
                subprocess.run(cmd, shell=True)
