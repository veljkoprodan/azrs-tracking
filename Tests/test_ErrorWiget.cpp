#include "ErrorWidget.h"
#include "catch.hpp"

TEST_CASE("Test ErrorWidget initialization")
{
  SECTION("ErrorWidget Constructor Test")
  {
    // Arrange

    // Act
    ErrorWidget* errorWidget = new ErrorWidget(nullptr);

    // Assert
    REQUIRE(errorWidget != nullptr);
  }
}
