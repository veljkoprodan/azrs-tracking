#ifndef GEOCODINGAPI_H
#define GEOCODINGAPI_H

#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QObject>

#include "ApiHandler.h"
#include "GeoLocationData.h"

class GeocodingAPI : public ApiHandler
{
  Q_OBJECT
 public:
  GeocodingAPI();
  ~GeocodingAPI() = default;

  void replyFinished(QNetworkReply *reply) override;

 signals:
  void geocodingDataUpdated(const QList<GeoLocationData> &locations);

 public slots:
  void geocodeCity(const QString &location);

 private:
  QString OPEN_CAGE_API_KEY;
};

#endif // GEOCODINGAPI_H
