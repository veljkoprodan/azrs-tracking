#ifndef DETAILEDWEATHERAPI_H
#define DETAILEDWEATHERAPI_H

#include <QGeoCoordinate>
#include <QNetworkAccessManager>
#include <QObject>
#include <QString>

#include "ApiHandler.h"
#include "GeoLocationData.h"

class DetailedWeatherData;

class DetailedWeatherAPI : public ApiHandler
{
  Q_OBJECT

 public:
  explicit DetailedWeatherAPI(QObject *parent = nullptr);
  ~DetailedWeatherAPI() = default;
  void fetchData(const GeoLocationData &location);

 private slots:
  void replyFinished(QNetworkReply *reply) override;

 signals:
  void dataFetched(const QSharedPointer<DetailedWeatherData> data);

 private:
  GeoLocationData location;
};

#endif // DETAILEDWEATHERAPI_H
