#ifndef ERRORWIDGET_H
#define ERRORWIDGET_H

#include <QLabel>

#include "BasicWidget.h"

class ErrorWidget : public BasicWidget
{
 public:
  explicit ErrorWidget(const QString &errorMessage, QWidget *parent = nullptr);

 private:
  QLabel *errorMessageLabel;
};

#endif // ERRORWIDGET_H
